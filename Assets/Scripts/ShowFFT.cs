﻿using UnityEngine;
using System.Collections;
using System;

public class ShowFFT : MonoBehaviour {
    
    AudioSource source;
    public GameObject prefab;
    public bool UseWindow = true;
    public bool UseHarris = false;
    public bool isScaledLog = false;
    GameObject[] instances = null;

    int nrOfSamples = 0;
    float[] data = null;

    int nrSamples = 8192;
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        data = new float[nrSamples];
        Spectrum = new float[nrSamples / 2];
        instances = new GameObject[nrSamples / 2];
        Vector3 newPos = prefab.transform.localPosition;
        for(int i = 0; i < instances.Length; i++)
        {
            newPos.x += 2;
            GameObject obj = Instantiate(prefab);
            instances[i] = obj;
            instances[i].transform.localPosition = new Vector3(newPos.x, newPos.y, newPos.z);
        }
        Debug.Log(source.clip.samples);
	}

    float[] Spectrum = null;
    int index = 0;
	// Update is called once per frame
	void Update () {
        nrOfSamples += nrSamples;
        nrOfSamples = nrOfSamples >= source.clip.samples ? 0 : nrOfSamples;
        source.clip.GetData(data, source.timeSamples); //nrOfSamples - nrSamples);
        STFT.Compute(data, 4096, 1024);
        if (STFT.Spectrum != null)
        {
            for (int i = 0; i < Spectrum.Length; i++)
            {
                if(UseHarris)
                {
                    if (UseWindow)
                    {
                        Spectrum[i] = STFT.Spectrum[index * STFT.GetSampleSizeSpectrum() + i];
                    }
                    else
                    {
                        Spectrum[i] = STFT.Peaks[index * STFT.GetSampleSizeSpectrum() + i];
                    }
                }
            }
        }

        index = (index < (STFT.GetFrameSize() - 1)) ? index + 1 : 0;

        for (int i = 0; i < Spectrum.Length; i++)
        {
            Vector3 scale = instances[i].transform.localScale;
            Vector3 pos = instances[i].transform.localPosition;
            scale.y = Mathf.Lerp(scale.y, Spectrum[i], Time.deltaTime * 20);
            pos.y = scale.y / 2;
            instances[i].transform.localScale = scale;
            instances[i].transform.localPosition = pos;
        }
    }


    public void useWindow(bool value)
    {
        UseWindow = value;
    }

    public void useHarris(bool value)
    {
        UseHarris = value;
    }
}
