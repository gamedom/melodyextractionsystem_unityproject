﻿using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour
{
    public ShowSpectrogram spectrogram;

    RectTransform rectTrans;

    float maxWidth;
    float currentWidth;

    // Use this for initialization
    void Start ()
    {
        rectTrans = GetComponent<RectTransform>();
        maxWidth = rectTrans.sizeDelta.x;
	}
	
	// Update is called once per frame
	void Update ()
    {
        float progress = (float)spectrogram.ClipOffset / spectrogram.ClipSamples;
        var size = rectTrans.sizeDelta;
        currentWidth = maxWidth * progress;
        size.x = currentWidth;
        rectTrans.sizeDelta = size;
	}
}
