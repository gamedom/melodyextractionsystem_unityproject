﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System;

public class SalienceExtraction : MonoBehaviour
{
    public const int SALIENCE_SIZE = 600;

    //#region Plugin related code

    enum ComputingStatus
    {
        Started = 1,
        Finished = 2,
        Idle = 3
    }

    private static bool isInitializedME = false;

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "InitializeME")]
    private static extern void InitializeME(int frames, int samples, int peakIndexesSize);

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "SendInputData")]
    private static extern int SendInputData(
        [In, Out] float[] spectrumFrame,
        [In, Out] float[] peaksFrame,
        [In, Out] int[] peaksIndexesFrame);

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "StartSalienceThread")]
    private static extern void StartSalienceThread();

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "GetSalienceComputingStatus")]
    private static extern int GetSalienceComputingStatus();

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "GetSalience")]
    private static extern void GetSalience(
        [In, Out] float[] salienceFrame);

    
    [DllImport("MelodyExtraction_CUDA", EntryPoint = "GetSalienceFiltered")]
    private static extern void GetSalienceFiltered(
        [In, Out] float[] salienceVoiced,
        [In, Out] float[] salienceUnvoiced);

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "DeinitializeME")]
    private static extern void DeinitializeME();

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "GetNFramesNSamples")]
    private static extern int GetNFramesNSamples(bool frames);

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "GetInputData")]
    private static extern int GetInputData(
        [In, Out] float[] spectrumFrame, 
        [In, Out] float[] peaksFrame, 
        [In, Out] int[] peaksIndexesFrame, 
        int frameIndex);

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "GetDebugCodeProgress")]
    private static extern float GetDebugCodeProgress();

    [DllImport("MelodyExtraction_CUDA", EntryPoint = "SetIdleSalienceThread")]
    private static extern void SetIdleSalienceThread();

    //#endregion

    [HideInInspector]
    public float[] Salience;

    [HideInInspector]
    public float[] SalienceVoiced;
    [HideInInspector]
    public float[] SalienceUnvoiced;

    public event Action OnFinishedExtraction;

    //#region public serialized fields

    public float updateDelay;
    
    //#endregion

    //#region private fields

    bool timedUpdate;

    //#endregion

    //#region public methods and properties

    public void ComputeSalience(float[] spectrum, float[] peaks, int[] peaksIndexes)
    {
        //InitializePlugin(spectrum.Length, spectrum[0].Length, peaksIndexes[0].Count);
        InitializePlugin(STFT.GetFrameSize(), STFT.GetSampleSizeSpectrum(), STFT.GetPeaksFoundIndexesSampleSize());

        int frames = GetNFramesNSamples(true);
        int samples = GetNFramesNSamples(false);

        //Debug.Log(string.Format("frames = {0}\nsamples = {1}", frames, samples));

        //for (int i = 0; i < spectrum.Length; i++)
        //{
            int response = SendInputData(spectrum, peaks, peaksIndexes);
            Assert.AreEqual(-1, response, "There is something wrong with sending input data to plugin.\nDOUBLE CHECK THE SIZES OF THE ARRAYS!!!");
        //}

        //float[] testSpectrum = new float[spectrum[0].Length];
        //float[] testPeaks = new float[peaks[0].Length];
        //int[] testPeaksIndexes = new int[peaksIndexes[0].Count];

        //frames = GetInputData(testSpectrum, testPeaks, testPeaksIndexes, 0);

        StartSalienceThread();
    }

    //#endregion

    //#region private methods
    private void InitializePlugin(int numOfFrames, int numOfSamples, int peakIndexesSize)
    {
        //Assert.IsFalse(isInitializedME, "Plugin is initialized already");
        if (!isInitializedME)
        {
            InitializeME(numOfFrames, numOfSamples, peakIndexesSize);
            isInitializedME = true;

            Salience = new float[numOfFrames * SALIENCE_SIZE];
            SalienceVoiced = new float[numOfFrames * SALIENCE_SIZE];
            SalienceUnvoiced = new float[numOfFrames * SALIENCE_SIZE];
            //for (int i = 0; i < numOfFrames; i++)
            //{
            //    Salience[i] = new float[SALIENCE_SIZE];
            //}
        }
    }

    private ComputingStatus GetStatus()
    {
        ComputingStatus status = ComputingStatus.Idle;

        int statusCode = GetSalienceComputingStatus();
        switch (statusCode)
        {
            case 1:
                status = ComputingStatus.Started;
                break;
            case 2:
                status = ComputingStatus.Finished;
                break;
            case 3:
                status = ComputingStatus.Idle;
                break;
            default:
                break;
        }

        return status;
    }

    private void GetComputingResults()
    {
        float[] results = new float[SALIENCE_SIZE * STFT.GetFrameSize()];
        float[] results2 = new float[SALIENCE_SIZE * STFT.GetFrameSize()];
        //for (int i = 0; i < Salience.Length; i++)
        //{
            GetSalience(results);
            results.CopyTo(Salience, 0);

        GetSalienceFiltered(results, results2);
        results.CopyTo(SalienceVoiced, 0);
        results2.CopyTo(SalienceUnvoiced, 0);
        //}
    }

    //#endregion

    //#region Coroutines
    IEnumerator TimedUpdate()
    {
        timedUpdate = true;
        yield return new WaitForSeconds(updateDelay);

        if (isInitializedME)
        {
            if (GetStatus() == ComputingStatus.Finished)
            {
                GetComputingResults();
                SetIdleSalienceThread();

                if (OnFinishedExtraction != null)
                    OnFinishedExtraction();
            }

            Debug.Log("debug status = " + GetDebugCodeProgress());
        }

        timedUpdate = false;
    }
    //#endregion

    //#region Monobehaviour methods
    // Use this for initialization
    void Start()
    {
        timedUpdate = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!timedUpdate)
            StartCoroutine(TimedUpdate());
    }

    void OnDestroy()
    {
        DeinitializeME();
        isInitializedME = false;
    }
    //#endregion
}
