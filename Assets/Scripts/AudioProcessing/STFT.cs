﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//This STFT uses a Hann Window
public class STFT
{
    //float[timeFrame][Bin]
    public static float[] Spectrum = null;
    public static float[] Peaks = null;
    public static int[] PeaksFoundIndexes = null;

    private static float[] window = null;
    private static float windowSum = 0.0f;
    private static float[] dataZeroPadded = null;
    private static int frameSize = 0;

    private static float[] OutFFT = null;

    private static int sampleSizeSpectrum = 0;
    private static int sampleSizeData = 0;
    private static int hopSize = 0;
    private static int peaksFoundIndexesSize = 0;
    private static int peaksFoundIndexesSampleSize = 0;
    private const int clusterSize = 16;

    public static int GetFrameSize()
    {
        return frameSize;
    }

    public static int GetSampleSizeSpectrum()
    {
        return sampleSizeSpectrum;
    }

    public static int GetSampleSizeData()
    {
        return sampleSizeData;
    }

    public static int GetHopSize()
    {
        return hopSize;
    }

    public static int GetPeaksFoundIndexesSampleSize()
    {
        return peaksFoundIndexesSampleSize;
    }

    public static void Compute(float[] data, int windowSize, int hopSize)
    {
        if (data == null || FFT.IsPowerOfTwo(data.Length) == false)
            return;

        if (Spectrum == null || window == null || window.Length != windowSize || sampleSizeData != data.Length )
        {
            Initialize(data.Length, windowSize, hopSize);
        }

        //for (int i = 0; i < PeaksFoundIndexes.Count; i++)
        //{
        //    PeaksFoundIndexes[i].Clear();
        //}
        //PeaksFoundIndexes.Clear();

        for (int i = 0; i < dataZeroPadded.Length; i++)
            dataZeroPadded[i] = 0.0f;

        for(int i = 0; i < frameSize; i++)
        {
            for(int j = 0; j < window.Length; j++)
            {
                dataZeroPadded[j] = window[j] * data[j + i * hopSize];
            }
            FFT.PowerSpectrum(dataZeroPadded.Length, dataZeroPadded, OutFFT);
            for(int k = 0; k < dataZeroPadded.Length / 2; k++)
            {
                Spectrum[i * sampleSizeSpectrum + k] = ComputeMagnitude(OutFFT[k]);
            }

            // Detection of local maxima
            //PeaksFoundIndexes.Add(new List<int>());
            for (int k = 0; k < dataZeroPadded.Length / 2; k += clusterSize)
            {
                float mean = 0f;
                for(int l = k; l < k + clusterSize; l++)
                {
                    mean += Spectrum[i * sampleSizeSpectrum + l];
                }
                mean /= clusterSize;
                
                for (int l = k; l < k + clusterSize; l++)
                {
                    Peaks[i * sampleSizeSpectrum + l] = (Spectrum[i * sampleSizeSpectrum + l] - mean) > 0 ? Spectrum[i * sampleSizeSpectrum + l] : 0;
                }

                float max = 0;
                int indexWithMaxPeak = 0;
                for (int l = k; l < k + clusterSize; l++)
                {
                    //max = max > Peaks[i * sampleSizeSpectrum + l] ? max : Peaks[i * sampleSizeSpectrum + l];
                    if (max < Peaks[i * sampleSizeSpectrum + l])
                    {
                        max = Peaks[i * sampleSizeSpectrum + l];
                        indexWithMaxPeak = l;
                    }
                }
                PeaksFoundIndexes[i * peaksFoundIndexesSampleSize + (k / clusterSize)] = indexWithMaxPeak;

                for (int l = k; l < k + clusterSize; l++)
                {
                    if ((Peaks[i * sampleSizeSpectrum + l] - max) >= 0)
                    {
                        Peaks[i * sampleSizeSpectrum + l] = Spectrum[i * sampleSizeSpectrum + l];
                        //PeaksFoundIndexes[i].Add(l);
                    }
                    else
                    {
                        Peaks[i * sampleSizeSpectrum + l] = 0;
                    }
                }
            }
        }
    }

    #region Internal
    private static void Initialize(int sampleSize, int windowSize, int hopSize)
    {
        if (!FFT.IsPowerOfTwo(windowSize) || hopSize < 1)
            return;

        window = new float[windowSize];
        for (int i = 0; i < window.Length; i++)
        {
            window[i] = 0.5f * (1 - Mathf.Cos(((float)FFT.M_2PI * i) / (window.Length - 1)));
            windowSum += window[i];
        }

        frameSize = ComputeFrameSize(sampleSize, windowSize, hopSize);
        sampleSizeData = sampleSize;
        sampleSizeSpectrum = sampleSizeData / 2;
        STFT.hopSize = hopSize;

        peaksFoundIndexesSize = (sampleSizeSpectrum / clusterSize) * frameSize;
        if (PeaksFoundIndexes == null || PeaksFoundIndexes.Length != peaksFoundIndexesSize)
        {
            PeaksFoundIndexes = new int[peaksFoundIndexesSize];
            peaksFoundIndexesSampleSize = sampleSizeSpectrum / clusterSize;
        }

        Spectrum = new float[frameSize * sampleSizeSpectrum];
        Peaks = new float[frameSize * sampleSizeSpectrum];
        //for (int i = 0; i < frameSize; i++)
        //{
        //    //Spectrum[i] = new float[sampleSize / 2];
        //    Peaks[i] = new float[sampleSizeSpectrum];
        //}

        dataZeroPadded = new float[sampleSizeData];
        OutFFT = new float[sampleSizeData];
    }

    private static int ComputeFrameSize(int sampleSize, int windowSize, int hopSize)
    {
        return (sampleSize - windowSize) / hopSize;
    }

    private static float ComputeMagnitude(float k)
    {
        return 2 * (Mathf.Abs(k) / windowSum);
    }
    #endregion
}
