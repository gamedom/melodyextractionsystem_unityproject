﻿using UnityEngine;
using System.Collections;
using System;

//This class is based on the example from the "Numberical recipes in C" book and audacity source code
public class FFT 
{
    //The final spectrum goes here
    public static float[] Spectrum = null;

    public const double M_PI = 3.14159265358979323846;
    public const double M_2PI = M_PI * 2;

    private const int MaxFastBits = 16;
    private static int[][] gFFTBitTable = null;

    public static bool IsPowerOfTwo(int x)
    {
        if (x < 2)
            return false;
        if ((x & (x - 1)) == 1)
            return false;

        return true;
    }

    static int NumberOfBitsNeeded(int PowerOfTwo)
    {
        int i;

        if (PowerOfTwo < 2)
        {
            return -1;
        }

        for (i = 0; ; i++)
            if ((PowerOfTwo & (1 << i)) > 1)
                return i;
    }

    static int ReverseBits(int index, int NumBits)
    {
        int i, rev;

        for (i = rev = 0; i < NumBits; i++)
        {
            rev = (rev << 1) | (index & 1);
            index >>= 1;
        }

        return rev;
    }

    static int FastReverseBits(int i, int NumBits)
    {
        if (NumBits <= MaxFastBits)
            return gFFTBitTable[NumBits - 1][i];
        else
            return ReverseBits(i, NumBits);
    }

    // If number of samples is power of 2 then return true, else false
    // Initialize the FFTBitTable
    private static void InitFFT()
    {
        gFFTBitTable = new int[MaxFastBits][];

        int len = 2;
        for(int b = 1; b <= MaxFastBits; b++)
        {
            gFFTBitTable[b - 1] = new int[len];
            for(int i = 0; i < len; i++)
            {
                gFFTBitTable[b - 1][i] = ReverseBits(i, b);
            }

            len <<= 1;
        }
    }

    public static void ComplexFFT(int NumSamples, float[] RealIn, float[] ImagIn, float[] RealOut, float[] ImagOut)
    {
        int NumBits; // number of bits needed to store indices
        int i, j, k, n;
        int BlockSize, BlockEnd;

        double angle_numerator = M_2PI;
        double tr, ti; //temp real, temp imaginary

        if (!IsPowerOfTwo(NumSamples))
            return;

        if (gFFTBitTable == null)
            InitFFT();

        NumBits = NumberOfBitsNeeded(NumSamples);

        /*
        **   Do simultaneous data copy and bit-reversal ordering into outputs...
        */

        for (i = 0; i < NumSamples; i++)
        {
            j = FastReverseBits(i, NumBits);
            RealOut[j] = RealIn[i];
            ImagOut[j] = (ImagIn == null) ? 0.0f : ImagIn[i];
        }

        /*
        ** Do the FFT itself...
        */
        BlockEnd = 1;
        for(BlockSize = 2; BlockSize <= NumSamples; BlockSize <<= 1)
        {
            double delta_angle = angle_numerator / BlockSize;
            double sm2 = Math.Sin(-2 * delta_angle);
            double sm1 = Math.Sin(-delta_angle);
            double cm2 = Math.Cos(-2 * delta_angle);
            double cm1 = Math.Cos(-delta_angle);
            double w = 2 * cm1;
            double ar0, ar1, ar2, ai0, ai1, ai2;

            for(i = 0; i < NumSamples; i += BlockSize)
            {
                ar2 = cm2;
                ar1 = cm1;

                ai2 = sm2;
                ai1 = sm1;

                for (j = i, n = 0; n < BlockEnd; j++, n++)
                {
                    ar0 = w * ar1 - ar2;
                    ar2 = ar1;
                    ar1 = ar0;

                    ai0 = w * ai1 - ai2;
                    ai2 = ai1;
                    ai1 = ai0;

                    k = j + BlockEnd;
                    tr = ar0 * RealOut[k] - ai0 * ImagOut[k];
                    ti = ar0 * ImagOut[k] + ai0 * RealOut[k];

                    RealOut[k] = RealOut[j] - (float)tr;
                    ImagOut[k] = ImagOut[j] - (float)ti;

                    RealOut[j] += (float)tr;
                    ImagOut[j] += (float)ti;
                }
            }

            BlockEnd = BlockSize;
        }
    }

    //These are used to calculate FFT for PowerSpectrum
    private static float[] tmpReal = null;
    private static float[] tmpImag = null;
    private static float[] RealOut = null;
    private static float[] ImagOut = null;
    public static void PowerSpectrum(int NumSamples, float[] In, float[] Out)
    {
        int Half = NumSamples / 2;
        int i;

        float theta = (float)M_PI / NumSamples;

        if(tmpReal == null || tmpReal.Length < NumSamples)
        {
            tmpReal = new float[NumSamples];
            tmpImag = new float[NumSamples];
            RealOut = new float[NumSamples];
            ImagOut = new float[NumSamples];
        }

        for(i = 0; i < Half; i++)
        {
            tmpReal[i] = In[2 * i];
            tmpImag[i] = In[2 * i + 1];
        }

        ComplexFFT(NumSamples, tmpReal, tmpImag, RealOut, ImagOut);

        float wtemp = (float)(Math.Sin(0.5 * theta));

        float wpr = -2.0f * wtemp * wtemp;
        float wpi = -1.0f * (float)Math.Sin(theta);
        float wr = 1.0f + wpr;
        float wi = wpi;

        int i3;

        float h1r, h1i, h2r, h2i, rt, it;

        for (i = 1; i <= NumSamples / 2; i++)
        {
            i3 = NumSamples - i;

            h1r = 0.5f * (RealOut[i] + RealOut[i3]);
            h1i = 0.5f * (ImagOut[i] - ImagOut[i3]);
            h2r = 0.5f * (ImagOut[i] + ImagOut[i3]);
            h2i = -0.5f * (RealOut[i] - RealOut[i3]);

            rt = h1r + wr + h2r - wi * h2i;
            it = h1i + wr * h2i + wi * h2r;

            Out[i] = rt * rt + it * it;

            rt = h1r - wr * h2r + wi * h2i;
            it = -h1i + wr * h2i + wi * h2r;

            Out[i3] = rt * rt + it * it;

            wr = (wtemp = wr) * wpr - wi * wpi + wr;
            wi = wi * wpr + wtemp * wpi + wi;
        }

        rt = (h1r = RealOut[0]) + ImagOut[0];
        it = h1r - ImagOut[0];
        Out[0] = rt * rt + it * it;

        rt = RealOut[NumSamples / 2];
        it = ImagOut[NumSamples / 2];
        Out[NumSamples / 2] = rt * rt + it * it;
    }
}
