﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading;
using System.Collections;

public class ShowSpectrogram : MonoBehaviour
{
    public SalienceExtraction salienceExtraction;
    public AudioClip clip;
    public AudioSource source;
    public GameObject prefab;
    public bool IsRealTime = false;
    public int sampleSize = 8192;
    public int windowSize = 2048;
    public int hopSize = 128;

    public int ClipOffset
    {
        get
        {
            return clipOffset;
        }
    }

    public int ClipSamples
    {
        get
        {
            return totalSamples;
        }
    }

    private Texture2D texture;
    private List<float[]> STFTData;
    private float[] data;

    private bool isComputeRoutineFinished = true;

    private int clipOffset;
    private int totalSamples;

    // Use this for initialization
    void Start()
    {
        data = new float[sampleSize];
        STFTData = new List<float[]>();
        clipOffset = 0;
        salienceExtraction.OnFinishedExtraction += OnFinishedExtractionCallback;
        totalSamples = source.clip.samples;
    }

    int samples = 0;
    // Update is called once per frame
    void Update()
    {
        if (IsRealTime)
        {
            if (source.timeSamples > samples)
            {
                RealTimeProcessClip();
                GenerateSpectogram();
                samples += sampleSize;
            }
        }
    }

    void OnDestroy()
    {
        salienceExtraction.OnFinishedExtraction -= OnFinishedExtractionCallback;
    }

    private void OnFinishedExtractionCallback()
    {
        for (int i = 0; i < STFT.GetFrameSize(); i++)
        {
            float[] result = new float[SalienceExtraction.SALIENCE_SIZE/*STFT.GetSampleSizeSpectrum()*/];
            //salienceExtraction.Salience[i]
            for (int j = 0; j < result.Length; j++)
            {
                result[j] = salienceExtraction.SalienceVoiced[i * SalienceExtraction.SALIENCE_SIZE + j];//STFT.Spectrum[i * STFT.GetSampleSizeSpectrum() + j];
            }
            STFTData.Add(result);
        }

        clipOffset += sampleSize;
        if (clipOffset < source.clip.samples)
        {
            source.clip.GetData(data, clipOffset);
            STFT.Compute(data, windowSize, hopSize);
            salienceExtraction.ComputeSalience(STFT.Spectrum, STFT.Peaks, STFT.PeaksFoundIndexes);
            Debug.Log("number of peaks found: " + STFT.GetPeaksFoundIndexesSampleSize());
        }
        else
        {
            Debug.Log("Finished to preprocess clip.");
        }

        Debug.Log("Finished Extraction.");
    }

    public void RealTimeProcessClip()
    {
        STFTData.Clear();

        if (source.clip == null)
        {
            Debug.LogError("Include an AudioClip in script");
            return;
        }

        if (data.Length != sampleSize)
        {
            data = null;
            data = new float[sampleSize];
        }

        //source.clip.GetData(data, source.timeSamples);
        //STFT.Compute(data, windowSize, hopSize);
        //for (int frameIndex = 0; frameIndex < STFT.GetFrameSize(); frameIndex++)
        //{
        //    STFTData.Add(STFT.Spectrum[frameIndex]);
        //}
    }

    public void PreprocessClip()
    {
        STFTData.Clear();
        if (source.clip == null)
        {
            Debug.LogError("Include an AudioClip in script");
            return;
        }

        Debug.Log("Total samples: " + source.clip.samples.ToString());

        if (data.Length != sampleSize)
        {
            data = null;
            data = new float[sampleSize];
        }

        clipOffset = 0;
        source.clip.GetData(data, clipOffset);
        STFT.Compute(data, windowSize, hopSize);
        salienceExtraction.ComputeSalience(STFT.Spectrum, STFT.Peaks, STFT.PeaksFoundIndexes);
        Debug.Log(STFT.GetFrameSize() + " : frame size");
    }

    public void GenerateSpectogram()
    {
        if (STFTData.Count < 1)
        {
            Debug.LogError("There is nothing to generate!!!");
            return;
        }

        int numOfPictures = 0;

        if (STFTData.Count > 16384)
        {
            numOfPictures = STFTData.Count / 16384;
            numOfPictures = STFTData.Count % 16384 > 0 ? numOfPictures + 1 : numOfPictures;
        }
        else
        {
            numOfPictures = 1;
        }


        if (numOfPictures == 1)
        {
            if (texture == null || texture.width != STFTData.Count || texture.height != STFTData[0].Length)
            {
                texture = new Texture2D(STFTData.Count, STFTData[0].Length, TextureFormat.ARGB32, true);
            }
        }
        else
        {
            if (texture == null || texture.width != 16384 || texture.height != STFTData[0].Length)
            {
                texture = new Texture2D(16384, STFTData[0].Length, TextureFormat.ARGB32, true);
            }
        }

        if (numOfPictures > 1)
        {
            for (int pictureNumber = 0; pictureNumber < numOfPictures; pictureNumber++)
            {
                int size = (STFTData.Count - (16384 * pictureNumber) >= 16384) ? 16384 : STFTData.Count - (16384 * pictureNumber);
                for (int frameIndex = 0; frameIndex < size; frameIndex++)
                {
                    float max = 0;
                    float avg = 0.0f;
                    for (int i = 0; i < STFTData[frameIndex + pictureNumber * 16384].Length; i++)
                    {
                        max = STFTData[frameIndex + pictureNumber * 16384][i] > max ? STFTData[frameIndex + pictureNumber * 16384][i] : max;
                        avg += STFTData[frameIndex + pictureNumber * 16384][i];
                    }
                    avg = avg / STFTData[frameIndex + pictureNumber * 16384].Length;

                    for (int sampleIndex = 0; sampleIndex < STFTData[frameIndex + pictureNumber * 16384].Length; sampleIndex++)
                    {
                        texture.SetPixel(frameIndex, sampleIndex, /*Color.yellow * */(STFTData[frameIndex + pictureNumber * 16384][sampleIndex] > avg ? Color.yellow : Color.black)/* * 50*/);
                    }
                }

                texture.Apply();
                byte[] dataFile = texture.EncodeToJPG();
                System.IO.File.WriteAllBytes("E:/spectrum_" + pictureNumber.ToString() + ".jpg", dataFile);
            }
        }
        else
        {
            for (int frameIndex = 0; frameIndex < STFTData.Count; frameIndex++)
            {
                float max = 0;
                float avg = 0.0f;
                for (int i = 0; i < STFTData[frameIndex].Length; i++)
                {
                    max = STFTData[frameIndex][i] > max ? STFTData[frameIndex][i] : max;
                    avg += STFTData[frameIndex][i];
                }
                avg = avg / STFTData[frameIndex].Length;

                for (int sampleIndex = 0; sampleIndex < STFTData[frameIndex].Length; sampleIndex++)
                {
                    texture.SetPixel(frameIndex, sampleIndex, /*Color.yellow * */(STFTData[frameIndex][sampleIndex] > avg ? Color.yellow : Color.black)/* * 50*/);
                }
            }

            texture.Apply();
            byte[] dataFile = texture.EncodeToJPG();
            System.IO.File.WriteAllBytes("E:/banana.jpg", dataFile);
        }
       
        Renderer rend = prefab.GetComponent<Renderer>();
        rend.material.mainTexture = texture;
    }

}
